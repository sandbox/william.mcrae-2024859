<?php

/*
 * @file
 * segment_io module.
 */

/**
 * Implements hook_admin_settings().
 */
function segment_io_admin_settings_form($form_state) {

  $options = array(0 => t('Hosted at Segment.io'), 1 => t('Standalone'));
  $form['segment_io_hosted'] = array(
    '#type' => 'radios',
    '#title' => t('Hosting'),
    '#default_value' => variable_get('segment_io_hosted', 0),
    '#options' => $options,
    '#description' => t('Do you have an account at Segment.io or are you managing it yourself.'),
    '#required' => TRUE,
  );

  $form['hosted'] = array(
    '#type' => 'fieldset',
    '#title' => t('Segment.io settings'),
  );
  $form['hosted']['segment_io_api_key'] = array(
    '#title' => t('API key'),
    '#type' => 'textfield',
    '#default_value' => variable_get('segment_io_api_key', ''),
    '#size' => 15,
    '#maxlength' => 20,
    '#required' => FALSE,
    '#description' => t('This is the API api of you Segment.io account, available from your account settings page at <a href="https://segment.io">segment.io</a>.'),
  );

  $form['standalone'] = array(
    '#type' => 'fieldset',
    '#title' => t('Standalone settings'),
  );

  $form['standalone']['segment_io_google_analytics_initialisation'] = array(
    '#title' => t('Google Analytics'),
    '#type' => 'textfield',
    '#default_value' => variable_get('segment_io_google_analytics_initialisation', ''),
    '#size' => 15,
    '#required' => FALSE,
    '#description' => t('Initialisation code, either an API key or a dictionary of setting in JSON format.')
  );

  return system_settings_form($form);
}
