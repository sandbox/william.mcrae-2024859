// Based on: https://segment.io/libraries/analytics.js#initialize

// Create a queue, but don't obliterate an existing one!
var analytics = analytics || [];

(function () {

    // A list of all the methods we want to generate queueing stubs for.
    var methods = [
    'identify', 'track', 'trackLink', 'trackForm', 'trackClick', 'trackSubmit',
    'page', 'pageview', 'ab', 'alias', 'ready', 'group'
    ];

    // For each of our methods, generate a queueing method that pushes arrays of
    // arguments onto our `analytics` queue. The first element of the array
    // is always the name of the analytics.js method itself (eg. `track`), so that
    // we know where to replay them when analytics.js finally loads.
    var factory = function (method) {
        return function () {
        analytics.push([method].concat(Array.prototype.slice.call(arguments, 0)));
        };
    };

    for (var i = 0; i < methods.length; i++) {
        analytics[methods[i]] = factory(methods[i]);
        }

    }());

jQuery.getScript("TOKEN_PATH", function(data, textStatus, jqxhr) {
    console.log(data); //data returned
    console.log(textStatus); //success
    console.log(jqxhr.status); //200
    console.log('Load was performed.');
    analytics.initialize(TOKEN_PROVIDERS_JSON);
});